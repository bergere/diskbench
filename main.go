package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/go-echarts/go-echarts/charts"
)

var benchArr []*Benchmark

type arguments struct {
	dirPaths     []string
	filesToWrite int
	fileSize     int
	withServer   bool
	port         string
}

func getArgs() arguments {
	args := arguments{}
	flag.IntVar(&args.filesToWrite, "files", 0, "amount of files to write")
	flag.IntVar(&args.fileSize, "size", 10, "write file size in MB")
	flag.BoolVar(&args.withServer, "serve", false, "use graphs server mode (default 'false')")
	dirs := flag.String("dir", ".", "comma ',' sepparated directories path to write and read")
	flag.StringVar(&args.port, "port", "8081", "graphs server port")
	flag.Parse()

	args.dirPaths = strings.Split(*dirs, ",")

	return args
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s", name, elapsed)
}

func lineChart(w http.ResponseWriter, _ *http.Request) {
	line := charts.NewLine()
	for _, bench := range benchArr {
		//bench.printResults()
		line.AddXAxis(bench.reader.readAmountMBPoints).
			AddYAxis(bench.dirPath+" read", bench.reader.readSpeedMBPoints,
				charts.LabelTextOpts{Show: false, Position: "bottom"}).
			AddYAxis(bench.dirPath+" write", bench.writer.writeSpeedMBPoints,
				charts.LabelTextOpts{Show: false, Position: "top"})
	}

	line.SetSeriesOptions(
		charts.MLNameTypeItem{Name: "Average", Type: "average"},
		charts.LineOpts{Smooth: true},
		charts.MLStyleOpts{Label: charts.LabelTextOpts{Show: true, Formatter: "{a}: {b}"}},
	)

	line.SetGlobalOptions(
		charts.TitleOpts{Title: "Read Write Performance"},
		charts.TitleOpts{Top: "bottom"},
		charts.ToolboxOpts{Show: true},
		//charts.InitOpts{Theme: charts.ThemeType.Chalk},
		charts.YAxisOpts{Name: "MB/s", SplitLine: charts.SplitLineOpts{Show: false}},
		charts.XAxisOpts{Name: "MB"},
	)
	line.Height = "80vh"
	line.Width = "95vw"

	f, err := os.Create("line.html")
	if err != nil {
		log.Println(err)
	}
	line.Render(w, f)
}

func main() {

	args := getArgs()
	log.SetOutput(os.Stdout)
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)

	var wg sync.WaitGroup
	for _, dir := range args.dirPaths {
		bench := Benchmark{
			dirPath:      dir,
			filesToWrite: args.filesToWrite,
			fileSizeMB:   args.fileSize,
		}
		wg.Add(1)
		go bench.run(&wg)
		benchArr = append(benchArr, &bench)
	}

	wg.Wait()
	if args.withServer {
		http.HandleFunc("/", lineChart)
		http.ListenAndServe(":"+args.port, nil)
	} else {
		for _, bench := range benchArr {
			bench.printResults()
		}
	}

}
