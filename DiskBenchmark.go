package main

import (
	"bufio"
	"crypto/rand"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"sync"
	"time"
)

// Benchmark provides disk performance measurement capabilities
type Benchmark struct {
	dirPath      string
	filesToWrite int
	fileSizeMB   int
	writer       BenchWriter
	reader       BenchReader
}

func (b *Benchmark) run(wg *sync.WaitGroup) {
	defer wg.Done()
	os.MkdirAll(b.dirPath, os.ModePerm)
	b.writer = BenchWriter{fileSizeMB: b.fileSizeMB}
	b.reader = BenchReader{}
	b.writer.generateFiles(b.dirPath, b.filesToWrite, b.writer.fileSizeMB)
	b.reader.iterrateDir(b.dirPath)
}

func (b Benchmark) printResults() {
	b.writer.printResults(b.dirPath)
	b.reader.printResults(b.dirPath)
}

///////////////////////// WRITER //////////////////////////////

// BenchWriter encapsulates all the write benchmark operations
type BenchWriter struct {
	writeDuration       float64
	dataInBytes         int64
	filesWritten        int
	fileSizeMB          int
	randomData          []byte
	writeAmountMBPoints []float64
	writeSpeedMBPoints  []float64
}

func (bw *BenchWriter) writeFile(filename string) error {
	err := ioutil.WriteFile(filename, bw.randomData, 0644)
	return err
}

func (bw *BenchWriter) generateFiles(dirPath string, filesToWrite, fileSizeMB int) {
	bw.randomData = make([]byte, fileSizeMB*1024*1024)
	rand.Read(bw.randomData)
	start := time.Now()
	for i := 1; i <= filesToWrite; i++ {
		fileName := fmt.Sprintf("file_%06d.dat", i)
		fileFullPath := path.Join(dirPath, fileName)
		bw.writeFile(fileFullPath)
		bw.filesWritten = i
		bw.writeDuration = time.Since(start).Seconds()
		bw.addStats()
		if i%5 == 0 {
			bw.printResults(dirPath)
		}
	}
}

func (bw *BenchWriter) addStats() {
	wroteMB := float64(bw.fileSizeMB * bw.filesWritten)
	bw.writeAmountMBPoints = append(bw.writeAmountMBPoints, wroteMB)
	writeSpeed := wroteMB / bw.writeDuration
	bw.writeSpeedMBPoints = append(bw.writeSpeedMBPoints, writeSpeed)
}

func (bw BenchWriter) printResults(dirPath string) {
	wa := len(bw.writeAmountMBPoints)
	ws := len(bw.writeSpeedMBPoints)

	if wa > 0 && ws > 0 {
		log.Printf("%s - Wrote: %6.3f MB in %6.3f seconds\n", dirPath, bw.writeAmountMBPoints[wa-1], bw.writeDuration)
		log.Printf("%s - Write Speed is: %6.3f MB/s\n", dirPath, bw.writeSpeedMBPoints[ws-1])
	}
}

////////////////////// READER //////////////////////////

// BenchReader encapsulates all the read benchmark operations
type BenchReader struct {
	readDuration       float64
	dataInBytes        int64
	fileSizeMB         int
	readAmountMBPoints []float64
	readSpeedMBPoints  []float64
}

func (br *BenchReader) addStats() {
	dataInMB := float64(br.dataInBytes) / (1024 * 1024)
	br.readAmountMBPoints = append(br.readAmountMBPoints, dataInMB)
	readSpeed := dataInMB / br.readDuration
	br.readSpeedMBPoints = append(br.readSpeedMBPoints, readSpeed)
}

func (br BenchReader) printResults(dirPath string) {
	ra := len(br.readAmountMBPoints)
	rs := len(br.readSpeedMBPoints)

	if ra > 0 && rs > 0 {
		log.Printf("%s - Read: %6.3f MB in %6.3f seconds\n", dirPath, br.readAmountMBPoints[ra-1], br.readDuration)
		log.Printf("%s - Read Speed is: %6.3f MB/s\n", dirPath, br.readSpeedMBPoints[rs-1])
	}
}

func (br BenchReader) readFile(filename string) (int64, error) {
	file, err := os.Open(filename)

	if err != nil {
		return 0, err
	}
	defer file.Close()

	stats, statsErr := file.Stat()
	if statsErr != nil {
		return 0, statsErr
	}

	size := stats.Size()
	bytes := make([]byte, size)

	bufr := bufio.NewReader(file)
	_, err = bufr.Read(bytes)

	return size, err
}

func (br *BenchReader) iterrateDir(dirPath string) {
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		log.Fatal(err)
	}

	// defer timeTrack(time.Now(), "read")
	br.dataInBytes = 0
	start := time.Now()
	for i, file := range files {
		fileFullPath := path.Join(dirPath, file.Name())
		size, _ := br.readFile(fileFullPath)
		br.dataInBytes += size
		br.readDuration = time.Since(start).Seconds()
		br.addStats()

		if i%5 == 0 {
			br.printResults(dirPath)
		}
	}
}
