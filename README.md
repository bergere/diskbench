# diskbench

##### Multi threaded disk benchmarking tool written in go

### Usage

```bash
mkdir /abcd/tmp #abcd is the tested drive mount
./diskbench -dir /abcd/tmp1,/abcd/tmp2 -files 1000 -size 8

./diskbench --help                          
Usage of ./diskbench:
  -dir string
        comma ',' sepparated directories path to write and read (default ".")
  -files int
        amount of files to write (default 0)
  -serve
        use web server on port 8081 (default 'false')
  -size int
        write file size in MB (default 10)
```

